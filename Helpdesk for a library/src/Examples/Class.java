//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.01.11 at 01:55:33 AM GMT 
//


package Examples;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Class complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Class">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="course" type="{}Course"/>
 *         &lt;element name="dateOfClass" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="taughtBy" type="{}Trainer"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="studentsAttending" type="{}Student" maxOccurs="unbounded"/>
 *         &lt;element name="studentsMissing" type="{}Student" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Class", propOrder = {
    "id",
    "course",
    "dateOfClass",
    "taughtBy",
    "name",
    "studentsAttending",
    "studentsMissing"
})
public class Class {

    @XmlElement(required = true)
    protected BigInteger id;
    @XmlElement(required = true)
    protected Course course;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateOfClass;
    @XmlElement(required = true)
    protected Trainer taughtBy;
    @XmlElement(required = true)
    protected String name;
    @XmlElement(required = true)
    protected List<Student> studentsAttending;
    @XmlElement(required = true)
    protected List<Student> studentsMissing;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setId(BigInteger value) {
        this.id = value;
    }

    /**
     * Gets the value of the course property.
     * 
     * @return
     *     possible object is
     *     {@link Course }
     *     
     */
    public Course getCourse() {
        return course;
    }

    /**
     * Sets the value of the course property.
     * 
     * @param value
     *     allowed object is
     *     {@link Course }
     *     
     */
    public void setCourse(Course value) {
        this.course = value;
    }

    /**
     * Gets the value of the dateOfClass property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfClass() {
        return dateOfClass;
    }

    /**
     * Sets the value of the dateOfClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfClass(XMLGregorianCalendar value) {
        this.dateOfClass = value;
    }

    /**
     * Gets the value of the taughtBy property.
     * 
     * @return
     *     possible object is
     *     {@link Trainer }
     *     
     */
    public Trainer getTaughtBy() {
        return taughtBy;
    }

    /**
     * Sets the value of the taughtBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link Trainer }
     *     
     */
    public void setTaughtBy(Trainer value) {
        this.taughtBy = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the studentsAttending property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the studentsAttending property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStudentsAttending().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Student }
     * 
     * 
     */
    public List<Student> getStudentsAttending() {
        if (studentsAttending == null) {
            studentsAttending = new ArrayList<Student>();
        }
        return this.studentsAttending;
    }

    /**
     * Gets the value of the studentsMissing property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the studentsMissing property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStudentsMissing().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Student }
     * 
     * 
     */
    public List<Student> getStudentsMissing() {
        if (studentsMissing == null) {
            studentsMissing = new ArrayList<Student>();
        }
        return this.studentsMissing;
    }

}
