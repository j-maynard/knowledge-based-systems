//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2016.01.11 at 01:55:33 AM GMT 
//


package Examples;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the Examples package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Mytrainingcompany_QNAME = new QName("", "mytrainingcompany");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: Examples
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TrainingCompany }
     * 
     */
    public TrainingCompany createTrainingCompany() {
        return new TrainingCompany();
    }

    /**
     * Create an instance of {@link Address }
     * 
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link Trainer }
     * 
     */
    public Trainer createTrainer() {
        return new Trainer();
    }

    /**
     * Create an instance of {@link Class }
     * 
     */
    public Class createClass() {
        return new Class();
    }

    /**
     * Create an instance of {@link Course }
     * 
     */
    public Course createCourse() {
        return new Course();
    }

    /**
     * Create an instance of {@link Student }
     * 
     */
    public Student createStudent() {
        return new Student();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TrainingCompany }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "mytrainingcompany")
    public JAXBElement<TrainingCompany> createMytrainingcompany(TrainingCompany value) {
        return new JAXBElement<TrainingCompany>(_Mytrainingcompany_QNAME, TrainingCompany.class, null, value);
    }

}
